# PART04

## 一、useModel

在PART03中，我们提到了使用整合了 useEvent 的`designComponent` 函数重构 `Input`组件，实现双向绑定的功能。既然提到了数据双向绑定，那我们先来了解一下`Vue2.0`中，响应式变量的一些坑。

> Vue3.0是基于Proxy代理对象实现属性劫持，而Vue2.0是基于 Object.defineProperty 实现的，最终Vue3.0在兼容IE浏览器的时候，很可能会用回 Vue2.0 的 Object.defineProperty 实现属性劫持。既然如此，在 `Vue2.0`中我们需要避免的坑，同样在3.0中同样需要避免。所以这一次训练的主题是：**`我们在设计组件的时候，尽量帮助开发者避免这些坑，同时我们自己在开发组件以及实现页面的时候，也要注意避免这些坑。`**

### 1、第一个示例

先来看第一个示例：[http://plain-pot.gitee.io/plain-ui-doc/run.html?path=components/demo/demo&index=0](http://plain-pot.gitee.io/plain-ui-doc/run.html?path=components/demo/demo&index=0)

> 这是一个比较常见的示例，在data函数中定义了一个表单数据对象：`formData:{}`，在`template`中，表单组件双向绑定了表单对象中的字段：`<input type="text" v-model="formData.name"/>` 以及 `<pl-input type="text" v-model="formData.name"/>`；

- 首先，直接在任意一个输入框中输入数据，是正常的，另一个输入框也能响应到新的数据，并且输入的数据也能够通过插值表达式显示到页面上。
- 第二步，刷新浏览器页面，使得页面重新初始化。这一次先点第一个按钮 `初始化name值`。你会发现值没有更新到页面上，接着你继续在输入框中输入数据，你会发现此时两个输入框不但没有显示新值，并且这时候你在任何一个输入框中输入内容，另一个输入框以及页面上的插值表达式也没有更新内容，但是你点击“打印formData（请看控制台）”按钮，你会发现此时 `formData.name`是值确实是新的，只是没有显示的到页面上。请自己收集资料，这里作为`【作业1】`训练，下一次文档发布前公布答案。
- 第三步。同样是先刷新浏览器页面，接着先点击 `初始化formData`按钮，此时你会发现页面上显示了新值，并且在任何一个输入框中输入内容，都是正常的。请解释为什么这种方式可以正常运行，对比`作业一`的区别是什么，这个作为`【作业2】`训练。
- 第四步，同样是先刷新浏览器页面，接着点击`初始化表单数据`按钮，等待按钮的加载状态结束，接着你会发现，页面上显示了新的值，但是此时你在任意一个输入框中输入内容，另一个输入框以及插值表达式并没有显示新值。请解释为什么。这里作为`【作业3】`训练；
- 第五步，刷新浏览器页面，点击`初始化formData2`按钮，等待按钮加载状态结束。此时你会发现一切正常。请解释这里与`作业3`的区别是什么，为什么这种做法能够正常运行，这个作为`【作业4】`训练。

> 这里一共有四个作业，如果你都能很清晰地答出来，你设计的组件基本上没有数据绑定的问题，否则你应该检查一下你写过的页面，有没有这种隐含的问题。

相关资料：

- [Vue.set](https://cn.vuejs.org/v2/api/#Vue-set)
- [vm.$set](https://cn.vuejs.org/v2/api/#vm-set)

### 2、第二个示例：多值绑定

先来看看`ElementUI`中，日期组件范围选择的用法：[http://plain-pot.gitee.io/plain-ui-doc/run.html?path=components/demo/demo&index=1](http://plain-pot.gitee.io/plain-ui-doc/run.html?path=components/demo/demo&index=1)

> 可以看到，当选择日期范围的时候，绑定值是一个数组。我第一次使用这个组件选择日期范围的时候，也是挺诧异的，为什么绑定的是数组。而我们在日常开发过程中，更多的是需要将值绑定到表单对象上，而不是绑定到一个数组变量中。

接下来看第二个示例:[http://plain-pot.gitee.io/plain-ui-doc/run.html?path=components/demo/demo&index=2](http://plain-pot.gitee.io/plain-ui-doc/run.html?path=components/demo/demo&index=2)

因为 `el-date-picker` 绑定的是数组，为了实现将值绑定到表单对象的两个字段上，这个示例里面写了几种办法（当然可能不是最好的办法，不过应该是大多数Vue开发者经常使用的方法）。

- 首先看`Element DEMO1`，我们都知道，`v-model` 是一个语法糖。也就是说，`<input v-model="formData.name"/>` 几乎等价于 `<input :value="formData.name" @input="val=>formData.name = val"/>`（实际上是不等价的，等价写法是，在@input事件中，首先判断 formData 对象的name属性是否存在，不存在则使用$set函数赋值，存在则直接赋值）。既然 `el-date-picker`绑定的是一个数组，那么我们通过自定义 `:value` 以及 `@input` 的值来实现将数组中的字段绑定到表单对象的属性上，这个示例里面是正常的，因为我们事先在定义formData表单对象的时候，定义了 `startDate` 以及 `endDate` 两个字段。
- 接着看`Element DEMO2`，我们平时在开发页面的时候，一般都是直接将 formData 赋值为后端返回的数据对象。而且我们没法保证后端返回的数据对象包含所需要的所有字段（有的接口为了节省流量传输，某个字段没有值，则返回的对象中直接就是没有这个字段）。我们总不能在将后端返回的数据对象赋值给 formData的时候，自己再手动初始化所有的字段，少一点的还好，像我经历的项目，一个页面上有百个字段是经常的事，并且系统中拥有数百个表单页面，每个页面初始化都需要给字段初始化，会累死人。DEMO2就是这种场景，你会发现选择完数据之后，页面上没有显示，插值表达式没有更新。但是此时点击旁边的“打印数据”按钮，你会发现数据确实是最新的。
- 接着看`Element DEMO3`，这个是正常的，对比DEMO2的赋值方式，再思考一下为什么DEMO2是有问题的。
- 接着看`Element DEMO3 ComputedModel`，这个是为了简化template中的写法，通过 computed 计算属性的get以及set，来实现将表单对象字段绑定到数组变量。
- 接着看`Plain DEMO04`，这个是正常的，即使没有在formData中定义这个字段。

--- 

- `【作业5】`：联系`作业1`，解释为什么DEMO2中的写法有问题。
- `【作业6】`：为什么 DEMO4 没有问题。


### 3、实现useModel组合函数

我们在PART03中，实现 Input 组件的双向数据绑定功能的时候，提示是这样的:

- 定义一个临时变量：`modelValue = ref(props.modelValue)`；
- 将 `modelValue.value` 传递给input元素的value属性；
- 监听`<input/>`元素的input事件，事件处理函数中，更新 modelValue.value 以及调用 `event.emit.updateModelvalue(modelValue.value)`派发事件update:modelValue更新绑定值；
- watch 监听 props.modelValue，当值变化时，更新 modelValue.value 为新值。

实际上，我们会有很多组件会有数据双向绑定的功能，多的数不胜数。每个组件都要套用这个写法，会很累，
而且容易出错。那么我们现在来封装这一系列的步骤成为一个组合函数。

##### `【作业7】`
根据上面的写法自己封装一个 `useModel`函数，并且能够有正确的类型提示。这个函数的使用形式是这样的：

```tsx
export default designComponent({
    name: 'pl-input',
    props: {
        modelValue: {},
    },
    emits: {
        updateModelValue: (val: any) => true
    },
    setup({props, event: {emit}}) {
        const model = useModel(() => props.modelValue, emit.updateModelValue)
        return {
            render: () => {
                return (
                    <input type="text" v-model={model.value}/>
                )
            }
        }
    },
})
```

##### `【作业8】`

实现一个组件`demo-use-model-component`，这个组件支持绑定单值，并且如果设置了 `range`属性为true时，可以绑定多值。这个组件的使用示例代码：

```html
<template>
    <div class="demo-use-model">
        <h4>单项绑定</h4>
        <demo-use-model-component v-model="val"/>
        <demo-use-model-component v-model="val"/>
        <h4>多值绑定</h4>
        <demo-use-model-component range v-model:start="start" v-model:end="end"/>
        <demo-use-model-component range v-model:start="start" v-model:end="end"/>
    </div>
</template>
<script>
    import {DemoUseModelComponent} from "./demo-use-model-component";
    export default {
        name: "demo-use-model",
        components: {
            DemoUseModelComponent,
        },
        data() {
            return {
                val: 'val',
                start: 'start',
                end: 'end'
            }
        },
    }
</script>
```

效果图：

![demo-use-model-component](./demo-use-model-component.gif)

## 二、总结

- `useModel`函数并不能解决开头所述的一些坑。只是简化我们实现绑定值的方式。
- 如果我们的组件绑定多值，在Vue2.0中应该尽量使用 `:xxx.synx` 实现多值绑定，尽量不要像 `el-date-picker`一样，绑定的是一个数组，因为这个数组中每一个索引位置的值代表的含义是不一样的，与 `CheckboxGroup` 绑定的字符串数组不一样，它的数组中每个元素代表的含义是一样的。像 `el-date-picker`这种绑定数组的方式，会增加开发者的心智负担。当然在Vue3.0中，如果绑定多值尽量设计成`v-model:xxx`这样的api用法，减少开发者负担。

--- 

如果我们的组件绑定的是多值，而且字段是动态的，这个如何实现？

请看这个页面：[http://martsforever.gitee.io/plain-ui-web/?id=plain-table-basic](http://martsforever.gitee.io/plain-ui-web/?id=plain-table-basic)；在这个示例页面中，其中的数据表格，倒数第三列“对象选择框”，这个组件实际上就是绑定了多个字段；这个列的代码时这样的：

```html
<pl-tc-object field="objName" title="对象选择框" :prop="{option:tableAddressOption,map:{obj:'id',objName:'name'}}"/>
```

这个是列的写法，如果换成表单写法，是这样的：

```html
<pl-object
        :row="formData"
        :map="{obj:'id',objName:'name'}"
        :option="tableAddressOption"
/>
```

这个组件，通过`row`属性接收一个表单对象，当弹框选择一条数据之后，通过map属性对象，将选中行的数据，映射赋值到 formData 对象中。此时由于formData中的字段可能没有初始化，所以在`<pl-object/>`中修改`row`对象时，
需要通过使用 `set` 函数来给 `row` 赋值。

## 三、作业

完成文档中描述的 作业1,2,3,4,5,6,7,8。