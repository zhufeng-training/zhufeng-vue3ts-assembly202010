# PART05

## 一、useStyle

### 概述

useStyle 是一个用来传递样式的组合函数。这样一句简短的说明，还不足够说清楚useStyle的实际作用。那我们先介绍另一个组合函数：useEdit。

### useEdit

在组件开发中，还有另一个组合函数与useStyle类似，叫做useEdit。我们都知道，几乎所有的表单组件都会有 `disabled, readonly`属性，以及大多数基于 `Input` 组件封装的输入框组件（Select、Cascade、Date、Time等等），都会有空值占位符参数：`placeholder`；

在这些二次封装的组件中，我们都得吧 `disabled, readonly, placeholder` 这三个值传递给 `Input` 组件，使得 `Input`组件能够按照父组件中接收的属性来展示当前的状态。比如我们给 `Select`组件设置了 `disabled=true`，那么此时 `Select`组件内的的`Input`组件应该表现为禁用状态。并且这个属性是响应式的。

useEdit组合函数，就是为了实现这三个属性在各种各样的二次封装组件中，透明传递 `disabled, readonly, placeholder`这三个属性。比如我们给 `ButtonGroup`设置了禁用，那么其下所有的`Button`组件都会处于禁用状态，如果我们给其中某一个`Button`设置了 `disabled=false`，那么除了这个按钮没有禁用之外，其他所有按钮都会按照父组件接收的 disabled 值来展示当前的状态。

> 具体效果可以看这个文档说明：[http://plain-pot.gitee.io/plain-ui-doc/#components%2Fform%2Fcontrol](http://plain-pot.gitee.io/plain-ui-doc/#components%2Fform%2Fcontrol)

### useStyle

useStyle也是这样一个组合函数，用来透明传输 `status, shape, size` 这三个属性。当你给 `Form`组件设置了 `size=large` 之后，所有的子组件都会展示位 `large` 尺寸大小。但你设置某个 `FormItem`的`status`为`error`的时候，其下所有的useStyle组件都会呈现为`error`状态。

> 因为 useEdit 内部有其他的逻辑，实现起来要比 useStyle 复杂很多，所以这里我们先实现 useStyle 函数。

### 作业一

手写实现 useStyle。实现如下效果：

![use-style](./use-style.png)

- 图片中的第一个示例，是在 parent 组件中使用 child组件的基本用法
- 图片中的第二个示例，是演示设置 parent 组件处于error状态，其子组件自动显示为error状态的示例。
- 图片中的第三个示例，是单独设置某个子组件为error状态的示例。
- 图片中的第四个示例，是父组件为error状态，其中第一个子组件为success状态，其他子组件自动使用parent的error状态的示例。

页面代码

```html
<template>
    <div class="demo-use-style">

        <demo-use-style-parent>
            <demo-use-style-child/>
            <demo-use-style-child/>
            <demo-use-style-child/>
        </demo-use-style-parent>

        <demo-use-style-parent status="error">
            <demo-use-style-child/>
            <demo-use-style-child/>
            <demo-use-style-child/>
        </demo-use-style-parent>

        <demo-use-style-parent>
            <demo-use-style-child status="error"/>
            <demo-use-style-child/>
            <demo-use-style-child/>
        </demo-use-style-parent>

        <demo-use-style-parent status="warn">
            <demo-use-style-child status="success"/>
            <demo-use-style-child/>
            <demo-use-style-child/>
        </demo-use-style-parent>
    </div>
</template>

<script>

    import {DemoUseStyleParent} from "./demo-use-style-parent";
    import {DemoUseStyleChild} from "./demo-use-style-child";

    export default {
        name: "demo-use-style",
        components: {
            DemoUseStyleParent,
            DemoUseStyleChild,
        },
    }
</script>

<style lang="scss">
    @include theme {
        .pl-use-style-parent {
            padding: 20px;
            margin: 10px 0;
            @include statusMixin(use-style-parent) {
                background-color: mix($value, white, 20%);
            }
        }

        .pl-use-style-child {
            padding: 8px;
            margin: 8px 0;
            @include statusMixin(use-style-child) {
                background-color: $value;
                color: white;
            }
        }
    }
</style>
```

useStyle 使用示例：

**demo-use-style-parent.tsx**

```tsx
import {designComponent} from "../../../../src/use/designComponent";
import {StyleProps, StyleStatus, useStyle} from "../../../../src/use/useStyle";
import {computed} from 'vue';
import {useSlots} from "../../../../src/use/useSlots";

export const DemoUseStyleParent = designComponent({
    props: {
        ...StyleProps,
    },
    setup() {

        const {slots} = useSlots()
        const {styleComputed} = useStyle({status: StyleStatus.primary})
        const classes = computed(() => [
            'pl-use-style-parent',
            `pl-use-style-parent-status-${styleComputed.value.status}`
        ])

        return {
            render: () => (
                <div class={classes.value}>
                    <div>PARENT</div>
                    <div>
                        {slots.default()}
                    </div>
                </div>
            )
        }
    },
})
```

> 其中的 `useStyle({status: StyleStatus.primary})` 表示默认值为 primary

**demo-use-style-child**

```html
import {designComponent} from "../../../../src/use/designComponent";
import {StyleProps, StyleStatus, useStyle} from "../../../../src/use/useStyle";
import {computed} from 'vue';

export const DemoUseStyleChild = designComponent({
    props: {
        ...StyleProps,
    },
    setup() {

        const {styleComputed} = useStyle({status: StyleStatus.primary})
        const classes = computed(() => [
            'pl-use-style-child',
            `pl-use-style-child-status-${styleComputed.value.status}`
        ])

        return {
            render: () => (
                <div class={classes.value}>
                    CHILD
                </div>
            )
        }
    },
})
```

> 其中的 `useStyle({status: StyleStatus.primary})` 表示默认值为 primary

**相关常量**

```tsx
export enum StyleShape {
    fillet = 'fillet',
    round = 'round',
    square = 'square',
}

export enum StyleSize {
    normal = 'normal',
    large = 'large',
    mini = 'mini',
}

export enum StyleStatus {
    primary = 'primary',
    success = 'success',
    error = 'error',
    warn = 'warn',
    info = 'info',
}

export const StyleProps = {
    shape: {type: String},                      // fillet,round,square
    size: {type: String},                       // normal,large,mini
    status: {type: String},                     // primary,success,error,warn,info
}
```

**提示**

- 通过 provide/inject 实现属性传递；
- 在 useStyle 中先inject父组件提供的值，返回一个 computed 计算属性 `styleComputed`，在计算属性中，如果props中有值则使用props中的值，否则如果inject对象有值则使用inject中的值，最后使用默认值；
- 最后将计算得到的 `styleComputed` provide出去；
- 返回 styleComputed ，组件中使用 styleComputed 中计算得到的 `status, size, shape` 来定义样式。

## 二、useSlots

### 概述

在Vue开发中，tempplate写法比jsx写法方便这是毋庸置疑的，可是template有两个缺点：
- 必须在 `.vue`单文件中开发；
- template 中的代码没有 typescript 类型约束。

在组件库开发中，这两个缺点都是挺致命的。前者会导致我们写的组件无法导出类型声明。我们必须给 `.vue`单文件组件另外写一份类型声明文件，如果不仔细检查，会导致组件的类型，与组件运行时所使用变量类型不一致。后者会导致我们在重构代码的时候，无法预知重构带来的风险。比如我们现在要重构 `deisgnComponent`函数。在 typescript 的帮助下，我们可以很快速地定位到所有受到影响的组件以及文件。特别是大型项目多人开发的情况下，严格的typescript类型约束会带来显著的效率上的提升。


在 template模式下，组件的插槽可以有预备内容，比如下面这段代码：

```html
<button type="submit">
  <slot>Submit</slot>
</button>
```

当调用者没有传递插槽内容时，显示为 `Submit` 字符串，当传递插槽内容时，会显示插槽内容。在tsx中，则必须得使用这种
看起来不友好的三元表达式写法：

```tsx
<button class={classes.value} onClick={event.emit.click}>
    {!setupContext.slots.default ? props.label : setupContext.slots.default()}
</button>
```

现在我们封装一个 `useSlots` 函数，目的有两个：

- 规范组件中使用插槽的方式，所有可能用到的插槽，都需要通过 `useSlots`定义，以便组件的维护人员在浏览器组件代码的时候，能够立刻分辨出组件支持的插槽有哪些。而不是在render函数中，一个一个地去搜索 slots 关键字。
- 因为插槽没有作用域参数，所以通过执行插槽函数获取插槽内容，这种方式有点多余。这里我们改造用法为：函数接收一个类型为 VNodeChild的 vnode 参数，当存在插槽时，渲染插槽内容，否则渲染vnode，也就是说，插槽函数的第一个参数为预备内容。

### 使用示例

```tsx
import {designComponent} from "../../../../src/use/designComponent";
import {useSlots} from "../../../../src/use/useSlots";
import {computed} from 'vue';

export const DemoUseSlotsComponent = designComponent({
    name: 'demo-use-slots-component',
    props: {},
    setup() {

        const {slots} = useSlots([
            'head',
            'foot'
        ])

        const classes = computed(() => [
            'demo-use-slots-component',
            {
                'demo-use-slots-component-has-slot-head': slots.head.isExist()
            }
        ])

        return {
            render() {
                return (
                    <div class={classes.value}>
                        <div class="demo-use-slots-component-head">
                            {slots.head('default head')}
                        </div>
                        <div class="demo-use-slots-component-body">
                            {slots.default(
                                <h1>default body</h1>
                            )}
                        </div>
                        <div class="demo-use-slots-component-foot">
                            {slots.foot('default foot')}
                        </div>
                    </div>
                )
            },
        }
    },
})
```

- useSlots 接收一个字符串数组，通过泛型将字符串数组转化类字面量类型，返回一个slots对象，slots对象的属性就是这个字面量类型，除此之外默认增加一个 `default` 字面量类型。
- useSlots返回的slots对象，他的每一个值是一个函数，函数接收后备的插槽内容，当组件没有对应的插槽内容时，显示对应具名插槽的后备内容。函数还有一个静态属性：isExist函数，通过调用这个函数来判断当前组件是否存在某个插槽。
- slots每个对象都有正确的类型提示；

> 提示：setupContext.slots 以及 getCurrentInstance().slots 中的内容不是响应式变量，也就是说你无法在computed函数中判断 setupContext.slots.default 是否存在。需要在beforeUpdate钩子函数中，将其中的值保存到一个响应式变量中，以便组件能够根据插槽的存在情况，使用不同的样式。

### 作业二

实现 useSlots函数，使得上述组件能够正常运行。

- 当不存在head插槽时，组件的head显示 `default head`字符串；
- 当存在head插槽时，显示head插槽内容，并且组件根节点加上`demo-use-slots-component-has-slot-head`类名；


## 三、useScopedSlots

### 概述

作用域插槽用的好不好，是衡量一个Vue开发者技术能力的重要指标。在封装基础组件的时候，作用域插槽显得没有那么重要，使用场景不多，也就 `Table`，`Tree`这些稍微复杂一点的组件会用到。

可是如果要你封装一些业务组件的时候，这个作用域插槽就显得尤为重要。这个涵盖PC、小程序以及基于Vue实现的混合式移动应用。

- 先来看PC，这个是PC上，基于基础表格封装的业务表格示例：[http://martsforever.gitee.io/plain-ui-web/?id=plain-table-basic](http://martsforever.gitee.io/plain-ui-web/?id=plain-table-basic)；基于这个基础表格，我们实现了一个新的业务表格，这个业务表格帮助我们：1）自动分页查询数据；2）自动管理新建、更新等编辑控制；3）自动切换行内编辑以及弹框表单编辑；4）数据筛选（这个示例里面只有一个搜索框筛选，实际上在我现在工作中封装的业务表格，支持的数据筛选方式有五六种，其中有类似于excel的组合筛选方式）；
- 接着看小程序以及基于Vue的混合式应用，我们可以基于作用域插槽封装一个自动查询列表组件，自动监听下拉刷新以及触底加载的功能。同时还具备列表多选、侧滑选项、筛选、排序以及搜索等功能。这些功能都可以集成到业务组件中。

上述两个复杂的业务组件都离不开Vue作用域插槽功能，通过作用域插槽，可以使得这些复杂多功能的业务组件具备个性化属性，满足各种各样的开发需求，提高开发人员的开发效率，同时减少页面bug的概率。我们都知道在设计软件系统的时候，尽量让用户选择而不是输入，同样的我们在给开发人员设计组件或者API的时候，尽量让开发人员少写或者不写代码，让开发者专注于业务逻辑。

> 实际应用开发中，基础组件需要我们开发的比较少，因为这些基础组件，像 `ElementUI, IView`这些优秀的组件库已经帮我们实现了，我们在公司业务中接触的最多的是这些业务组件，换句话说，实际上这个作用域插槽用的特别频繁。这个作用域插槽除了能够作用在这种表格、列表之外，还配合jsx实现一些复杂的组件服务调用，也是一个特别强力的帮手。

### 实现

useScopedSlots 与 useSlots一样，首先一个目的是规范组件的作用域插槽开发方式，所有组件中可能用到的作用域插槽，都需要在 useScopedSlots 函数中定义好才能使用。其次useScopedSlots在渲染作用域插槽的时候，需要传入作用域参数，言外之意我们还需要定义作用域参数的参数类型。除此之外，我们还有类似useSlots的后备插槽内容等功能。

所以我们希望的api大概是这样的，在调用函数渲染作用域插槽的时候，需要有两个参数，第一个参数是作用域参数，第二个是后备插槽内容。当然这两个参数顺序可以调换。

**使用形式**

```tsx
/*定义*/
const {scopedSlots} = useScopedSlots({
    default: {node: TreeNode, index: Number},
})

/*使用*/
scopedSlots.default(
    {node, index},
    (<button>{JSON.stringify(node.data)}</button>)
)
```

- 在定义插槽的时候，key为插槽名称，值为插槽的作用域变量类型；
- 在使用插槽的时候，第一个参数类型要符合定义的时候的参数类型；
- 
### 作业三

实现 useScopedSlots函数。

> 提示

可以参考useSlots的实现方式，除了调用的函数类型有点不一样之外，其他基本都是一样的；

相关类型定义：

```tsx
import {VNodeChild} from "../shims";
import {getCurrentInstance, reactive, onBeforeUpdate} from 'vue';

type ScopedSlotsOptionType = { [SlotName: string]: { [ScopeKey: string]: any } }

type ExtractScopedSlotsDataValue<ScopeValue> = ScopeValue extends new (...args: any[]) => infer R ? R : ScopeValue;

type ExtractScopedSlotsData<Scope extends { [ScopeKey: string]: any }> = {
    [k in keyof Scope]: ExtractScopedSlotsDataValue<Scope[k]>
}

type ScopedSlotsData<T extends ScopedSlotsOptionType> = {
    [k in keyof T]: ((scope: ExtractScopedSlotsData<T[k]>, vnode: VNodeChild) => void) & { isExist: () => boolean }
}

export function useScopedSlots<T extends ScopedSlotsOptionType>(options: T): { scopedSlots: ScopedSlotsData<T> } {
    ...
}
```

## 总结

PART05也是一个比较重要的部分，与上次的PART02不同，PART05更多的是强调作用域插槽的灵活使用。PART05我会录制一个视屏专门讲解作用域插槽的规范使用，以及灵活的用法。