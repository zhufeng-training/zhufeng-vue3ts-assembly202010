# PART LAST

视频地址:[https://www.bilibili.com/video/BV1sa411c7b7/](https://www.bilibili.com/video/BV1sa411c7b7/)

本期主要讲解内容：

- PART 03,04,05 回顾；
    - 03：useEvent；
    - 04：useModel；
    - 05：useStyle、useSlot、useScopedSlot；
- 组合函数综合运用；
    - Checkbox and CheckboxGroup
- 作用域插槽重要性；
    - 分析API用法。
- 组件服务的实现；
    - 动态挂载的缺点
    - 通过ROOT挂载，优缺点；
- 图标按需引入；
    - 实现原理
- 组件难度分析；
    - Table：★★★★★★★★★★：10
    - Tree:★★★★★★：6
    - Popper及其服务：★★★★★：5
    - Date：★★★★：4
    - Form：★★★：3
    - Scroll：★★：2
    - 组件服务：★★：2
    - 主题切换：★：1
    - designComponent：★：1