module.exports = {
    scss: {
        globalImport: [
            'src/style/global-import.scss',
        ],
        importOnce: [
            'src/style/public.scss',
        ]
    },
}