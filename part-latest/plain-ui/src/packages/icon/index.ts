import Icon from './icon'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(Icon)