import Loading from './loading'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(Loading)