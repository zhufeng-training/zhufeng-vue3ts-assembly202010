import Input from "./input";
import Icon from '../icon'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(Input, [Icon])