import Button from './button'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(Button)