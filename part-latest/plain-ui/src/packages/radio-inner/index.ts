import {createComponentPlugin} from "../../utils/createComponentPlugin";
import inner from './radio-inner'

export default createComponentPlugin(inner)