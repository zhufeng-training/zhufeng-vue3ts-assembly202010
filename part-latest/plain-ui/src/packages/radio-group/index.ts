import group from './radio-group'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(group)