import item from './item'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(item)