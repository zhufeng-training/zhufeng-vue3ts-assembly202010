# PART02

## 概述

本次实践为第二部分，即PART02，由于本次实践难度比较大，内容多而且有点复杂，本次时间为4天。这一次我们实践内容有以下几个内容：

- 实现一个定义组件的函数：designComponent，通过这个函数，可以使得我们在获取组件的引用（ref）以及注入（inject）组件提供（provide）的数据时，自动获取正确的类型提示以及约束，无需额外编写组件类型声明。
- 打包发布：
    - 打包输出一个umd格式的主要文件。
    - 在普通的html文件中引入这个文件，并且能够正常使用其中的组件；
    - 将打包好的umd文件以及源码发布到npm仓库上，在一个新的vue-cli工程中安装使用组件库，在使用组件的时候能够有正确的类型提示；并且支持按需引入组件。
    
视频地址：[https://www.bilibili.com/video/BV1BV411y7ax/](https://www.bilibili.com/video/BV1BV411y7ax/)    

## 相关资料

- 主题相关代码：见当前目录下的`资料/style`（由于时间有限，关于主题的实现原理讲解将会往后安排，这一期训练营结束之前会讲完主题相关内容）；
- 打包配置文件：见当前目录下的`资料/build`，有webpack以及rollup两种方式打包配置供参考，后续会安排时间讲解打包配置文件；

## 作业

- 简述 designComponent 意义何在；并且自己手写实现 designComponent 源码，理解其中每一个类型的含义（开始入门了解Vue3.0的类型系统）；
- 总结自己在实现designComponent过程中，对于Vue3.0的类型的理解，并且整理出自己在typescript这一块缺少的知识技能，对哪一块内容不够清晰。
- 使用designComponent函数实现简单的 input、button组件，并通过给出的主题样式文件，实现组件的主题色功能。使用 designComponent 重构 PART01
中的 `app-menu, AppNavigator, AppNavigatorPage`组件。
- 打包组件，在纯html文件中引入umd文件能够正常使用暴露的对象。将打包好的umd文件以及组件源码src目录发布到npm，新建一个新的vue-cli工程，
在这个新的工程中安装组库，并且能够正确获取组件的类型。
- 新工程中能够按需引入组件。
- 新工程中能够正确配置使用安装的组件库中的主题系统。

**安装打包所需要的依赖**

```bash
npm i autoprefixer@8 babel-loader@8 css-loader@5 mini-css-extract-plugin postcss@8 postcss-loader@4 rollup-plugin-postcss@3 rollup-plugin-terser sass sass-loader@8 ts-loader@8 typescript webpack@4 webpack-cli@4 webpack-merge @rollup/plugin-babel @rollup/plugin-commonjs @rollup/plugin-node-resolve @rollup/plugin-typescript -D
```